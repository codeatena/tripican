package com.han.showtime.api.asynctask;

import android.app.Activity;
import android.content.Context;

import com.han.api.MyAsyncTask;
import com.han.api.ParseResult;
import com.han.showtime.GlobalData;
import com.han.showtime.activities.MainActivity;
import com.han.showtime.activities.MovieActivity;
import com.han.showtime.activities.SplashActivity;
import com.han.showtime.api.parseresult.CinemasParseResult;
import com.han.showtime.api.parseresult.MoviesParseResult;
import com.han.showtime.api.parseresult.ShowTimesParseResult;
import com.han.showtime.api.parseresult.StatesParseResult;
import com.han.showtime.api.service.UserWebService;
import com.han.showtime.model.Cinema;
import com.han.showtime.model.CinemaShowTimes;
import com.han.showtime.model.Movie;
import com.han.utility.NetworkUtility;

import java.util.ArrayList;

public class UserAsyncTask extends MyAsyncTask {

	public static String ACTION_MOVIES = "action_movies";
    public static String ACTION_STATES = "action_states";
    public static String ACTION_CINEMAS = "action_cinemas";
    public static String ACTION_SHOWTIMES = "action_showtimes";

    public UserAsyncTask(Context _parent) {
		super(_parent);
		// TODO Auto-generated constructor stub
	}

	public UserAsyncTask(Context _parent, String _title) {
		super(_parent, _title);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected ParseResult doInBackground(String... params) {
		// TODO Auto-generated method stub

		ParseResult pResult = new ParseResult(false);

		curAction = params[0];

        if (NetworkUtility.getConnectivityStatus(parent) == NetworkUtility.TYPE_NOT_CONNECTED) {
            pResult.nType = ParseResult.ERROR;
            pResult.message = "Your phone is not connencted to internet. Please check your network status.";
            return pResult;
        }

		if (curAction.equals(ACTION_MOVIES)) {
            String strPagenNum = params[1];
			pResult = UserWebService.readAllMovies(params[1], params[2], params[3], params[4]);
		}
        if (curAction.equals(ACTION_STATES)) {
            pResult = UserWebService.readStates(params[1]);
        }
        if (curAction.equals(ACTION_CINEMAS)) {
            pResult = UserWebService.readCinemas(params[1]);
        }
        if (curAction.equals(ACTION_SHOWTIMES)) {
            pResult = UserWebService.readShowTimes(params[1], params[2], params[3], params[4]);
        }

		return pResult;
	}
	
	protected void onPostExecute(ParseResult pResult) {
		
		super.onPostExecute(pResult);

        if (pResult.isSuccess) {
            if (curAction.equals(ACTION_MOVIES)) {
                if (pResult.isSuccess) {
                    MainActivity mainActivity = (MainActivity) parent;
                    ArrayList<Movie> arrMovies = ((MoviesParseResult) pResult).arrMovies;
                    mainActivity.addMovies(arrMovies);
                }
            }
            if (curAction.equals(ACTION_STATES)) {
                if (pResult.isSuccess) {
                    GlobalData.getInstance().arrStates = ((StatesParseResult) pResult).arrLocation;
                    SplashActivity splashActivity = (SplashActivity) parent;
                    splashActivity.successGetStates();
                }
            }
            if (curAction.equals(ACTION_CINEMAS)) {
                if (pResult.isSuccess) {
                    ArrayList<Cinema> arrCinemas = ((CinemasParseResult) pResult).arrCinemas;
                    MainActivity mainActivity = (MainActivity) parent;
                    mainActivity.addCinemas(arrCinemas);
                }
            }
            if (curAction.equals(ACTION_SHOWTIMES)) {
                if (pResult.isSuccess) {
                    ArrayList<CinemaShowTimes> arrCinemaShowTimes = ((ShowTimesParseResult) pResult).arrCinemaShowTimes;
                    MovieActivity movieActivity = (MovieActivity) parent;
                    movieActivity.successReadShowTime(arrCinemaShowTimes);
                }
            }
        }
	}
}