package com.han.api;

import android.util.Base64;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import oauth.signpost.OAuth;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.basic.DefaultOAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import oauth.signpost.http.HttpParameters;


public class MyWebService {

	public static final int SUCCESS = 1;
	public static final int FAIL = -1;
	public static final int AUTHORIZATION_FAIL = -2;
	
	final public static String URL_API = "http://beta20.tripican.com/api/";
	
//	public static String callHttpRequestGeneral(String url, String method, List<NameValuePair> params) {
//        String strResponse = null;
//        try {
//            if (method == "POST") {
//
//                DefaultHttpClient httpClient = new DefaultHttpClient();
//                HttpPost httpPost = new HttpPost(url);
//                httpPost.setEntity(new UrlEncodedFormEntity(params));
//                HttpResponse httpResponse = httpClient.execute(httpPost);
//                InputStream inputStream = httpResponse.getEntity().getContent();
//    	        BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"), 8);
//                StringBuilder sBuilder = new StringBuilder();
//
//                String line = null;
//                while ((line = bReader.readLine()) != null) {
//                    sBuilder.append(line + "\n");
//                }
//
//                inputStream.close();
//                strResponse = sBuilder.toString();
//
//            } else if (method == "GET") {
//
//            	HttpClient httpClient = GetClient();
//                HttpGet httpGet = new HttpGet(url);
//                HttpResponse httpResponse = httpClient.execute(httpGet);
//
//                strResponse = EntityUtils.toString(httpResponse.getEntity());
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//
//        Log.e("response", strResponse);
//
//        return strResponse;
//    }
	
//	public static String callHttpRequestMultiPart(String url, MultipartEntity nameValuePairs) {
//		String strResponse = null;
//
//        try {
//	        HttpClient httpClient = new DefaultHttpClient();
//
//	        HttpPost httpPost = new HttpPost(url);
//	        httpPost.setEntity(nameValuePairs);
//
//	        HttpResponse httpResponse = httpClient.execute(httpPost);
//            strResponse = EntityUtils.toString(httpResponse.getEntity());
//
//	        Log.e("response", strResponse);
//        } catch (Exception e) {
//        	e.printStackTrace();
//        	return null;
//        }
//
//        return strResponse;
//	}
//
//	public static DefaultHttpClient GetClient() {
//		DefaultHttpClient ret = null;
//
//		// sets up parameters
//		HttpParams params = new BasicHttpParams();
//		HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
//		HttpProtocolParams.setContentCharset(params, "utf-8");
//		params.setBooleanParameter("http.protocol.expect-continue", false);
//
//		// registers schemes for both http and https
//		SchemeRegistry registry = new SchemeRegistry();
//		registry.register(new Scheme("http", PlainSocketFactory
//				.getSocketFactory(), 80));
//		registry.register(new Scheme("https", new EasySSLSocketFactory(), 443));
//		ThreadSafeClientConnManager manager = new ThreadSafeClientConnManager(
//				params, registry);
//		ret = new DefaultHttpClient(manager, params);
//		return ret;
//	}
	
	public static String getB64Auth(String login, String pass) {
		String source = login + ":" + pass;
		String ret = "Basic "
				+ Base64.encodeToString(source.getBytes(), Base64.URL_SAFE
						| Base64.NO_WRAP);
		return ret;
	}

	public static Map<String, String> splitQuery(String query) throws UnsupportedEncodingException {
        Map<String, String> queryPairs = new LinkedHashMap<String, String>();
        String[] pairs = query.split("&");
        for (String pair : pairs) {
            int idx = pair.indexOf("=");
            queryPairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
        }
        return queryPairs;
    }

    public static ApiResult getWithResult(String endpoint, OAuthToken token) throws ProtocolException, IOException, OAuthMessageSignerException, OAuthExpectationFailedException, OAuthCommunicationException {

        OAuthConsumer defaultConsumer = new DefaultOAuthConsumer(ConsumerConsts.CONSUMER_KEY, ConsumerConsts.CONSUMER_SECRET);

        if (token != null) {
            defaultConsumer.setTokenWithSecret(token.getToken(), token.getTokenSecret());
        }

        URL url = new URL(endpoint);

        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestProperty("Accept", "application/json");
        urlConnection.setRequestMethod("GET");
        //urlConnection.setDoOutput(true);
        defaultConsumer.sign(urlConnection);

        return getResult(urlConnection);
    }

    public static String get(String endpoint, OAuthToken token) throws ProtocolException, IOException, OAuthMessageSignerException, OAuthExpectationFailedException, OAuthCommunicationException {
        return getWithResult(endpoint, token).getResponse();
    }

    private static String encodeParams(HttpParameters params) {
        //Create the POST payload
        StringBuilder sb = new StringBuilder();
        Iterator<String> iter = params.keySet().iterator();
        for (int i = 0; iter.hasNext(); i++) {
            String param = iter.next();
            if (i > 0) {
                sb.append("&");
            }
            sb.append(param);
            sb.append("=");
            sb.append(OAuth.percentEncode(params.getFirst(param)));
        }

        //Send the payload to the connection
        return sb.toString();
    }

    public static String post(final String endpoint, List<NameValuePair> paramsPair, OAuthToken token) throws IOException, OAuthMessageSignerException, OAuthExpectationFailedException, OAuthCommunicationException {
        return postWithResult(endpoint, paramsPair, token).getResponse();
    }

    public static ApiResult postWithResult(final String endpoint, List<NameValuePair> paramsPair, OAuthToken token) throws IOException, OAuthMessageSignerException, OAuthExpectationFailedException, OAuthCommunicationException {

        //Create an HttpURLConnection and add some headers
        URL url = new URL(endpoint);

        HttpParameters params = new HttpParameters();
        if(paramsPair != null && !paramsPair.isEmpty()){
            for(NameValuePair nameValuePair : paramsPair){
                params.put(nameValuePair.getName(), nameValuePair.getValue());
            }
        }

        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestProperty("Accept", "application/json");
        urlConnection.setRequestMethod("POST");
        urlConnection.setDoOutput(true);

        //Sign the request
        //The key to signing the POST fields is to add them as additional parameters, but already percent-encoded; and also to add the realm header.
        HttpParameters doubleEncodedParams = new HttpParameters();
        Iterator<String> iter = params.keySet().iterator();
        while (iter.hasNext()) {
            String key = iter.next();
            doubleEncodedParams.put(key, OAuth.percentEncode(params.getFirst(key)));
        }

        doubleEncodedParams.put("realm", endpoint);

        OAuthConsumer defaultConsumer = new DefaultOAuthConsumer(ConsumerConsts.CONSUMER_KEY, ConsumerConsts.CONSUMER_SECRET);

        if (token != null) {
            defaultConsumer.setTokenWithSecret(token.getToken(), token.getTokenSecret());
        }

        defaultConsumer.setAdditionalParameters(doubleEncodedParams);
        defaultConsumer.sign(urlConnection);

        //Send the payload to the connection
        String formEncoded = encodeParams(params);
        OutputStreamWriter outputStreamWriter = null;
        try {
            outputStreamWriter = new OutputStreamWriter(urlConnection.getOutputStream(), "UTF-8");
            outputStreamWriter.write(formEncoded);
        } finally {
            if (outputStreamWriter != null) {
                outputStreamWriter.close();
            }
        }

        return getResult(urlConnection);
    }

    public static ApiResult getResult(HttpURLConnection urlConnection) throws IOException {

        //Send the request and read the output
        try {
            InputStream in;
            System.out.println("Response: " + urlConnection.getResponseCode() + " " + urlConnection.getResponseMessage());
            boolean success = false;
            if (urlConnection.getResponseCode() != 200) {
                in = new BufferedInputStream(urlConnection.getErrorStream());
            } else {
                in = new BufferedInputStream(urlConnection.getInputStream());
                success = true;
            }

            String inputStreamString = new Scanner(in, "UTF-8").useDelimiter("\\A").next();
            if (success) {
                return new ApiResult(inputStreamString);
            } else {
                try {
                    return ApiResult.fromErrors(inputStreamString);
                } catch (JSONException ex) {
                    ApiResult result = new ApiResult(inputStreamString);
                    result.getErrors().add(new ApiError("Could not parse error message.", "0"));
                    return result;
                }
            }
        } finally {
            urlConnection.disconnect();
        }
    }
}
