package com.han.showtime.model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by COREI3 on 5/5/2015.
 */

public class CinemaShowTimes {

    public Cinema cinema = new Cinema();
    public ArrayList<MovieShowTimes> arrMovieShowTimes = new ArrayList<MovieShowTimes>();

    public CinemaShowTimes parse(JSONObject jsonObj) {
        try {
            cinema = new Cinema().parse(jsonObj.getJSONObject("cinema"));
            JSONArray jsonArrMovieShowTimes = jsonObj.getJSONArray("movie_showtimes");

            arrMovieShowTimes = new ArrayList<MovieShowTimes>();

            for (int i = 0; i < jsonArrMovieShowTimes.length(); i++) {
                JSONObject jsonMovieShowTime = jsonArrMovieShowTimes.getJSONObject(i);
                MovieShowTimes movieShowTimes = new MovieShowTimes().parse(jsonMovieShowTime);
                arrMovieShowTimes.add(movieShowTimes);
            }

            return this;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}