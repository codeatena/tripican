package com.han.showtime.spinner;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.han.showtime.R;
import com.han.showtime.model.Cinema;
import com.han.showtime.model.Location;

import java.util.ArrayList;

/**
 * Created by COREI3 on 5/5/2015.
 */

public class MySpinnerAdapter extends ArrayAdapter<MySpinnerItem> {

    public MySpinnerAdapter(Context context, ArrayList<MySpinnerItem> spinnerItems) {
        super(context, 0, spinnerItems);
    }

    @Override
    public boolean isEnabled(int position) {
        // TODO Auto-generated method stub
        if (position == 0) {
            return false;
        }
        return true;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        View view = convertView;
        LayoutInflater inflater = LayoutInflater.from(getContext());

        MySpinnerItem item = getItem(position);

        if (item instanceof MySpinnerItem) {
            view = inflater.inflate(item.dropdownLayoutID, parent, false);

            Object data = item.data;
            if (data instanceof Location) {
                Location location = (Location) data;
                TextView txtLabel = (TextView) view.findViewById(R.id.label_textView);
                txtLabel.setText(location.name);
            } else if (data instanceof Cinema) {
                Cinema cinema = (Cinema) data;
                TextView txtLabel = (TextView) view.findViewById(R.id.label_textView);
                txtLabel.setText(cinema.centerName);
            }
        }

        return view;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        LayoutInflater inflater = LayoutInflater.from(getContext());

        MySpinnerItem item = getItem(position);

        if (item instanceof MySpinnerItem) {
            view = inflater.inflate(item.backLayoutID, parent, false);
            Object data = item.data;
            if (data instanceof Location) {
                Location location = (Location) data;
                TextView txtLabel = (TextView) view.findViewById(R.id.label_textView);
                txtLabel.setText(location.name);
            } else if (data instanceof Cinema) {
                Cinema cinema = (Cinema) data;
                TextView txtLabel = (TextView) view.findViewById(R.id.label_textView);
                txtLabel.setText(cinema.centerName);
            }
        }

        return view;
    }
}
