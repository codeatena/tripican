package com.han.showtime.movie;

import android.app.Fragment;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebChromeClient;

import com.han.showtime.R;
import com.han.showtime.activities.MovieActivity;
import com.han.utility.YoutubeUtility;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 * Created by COREI3 on 5/5/2015.
 */
public class MovieTrailerFragment extends Fragment {

    WebView webView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_movie_trailer, container, false);

        webView = (WebView) view.findViewById(R.id.webView);

        initValue();
        initEvent();

        return view;
    }

    private void initValue() {

        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setAllowFileAccess(true);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.getSettings().setUserAgentString("ua");
        webView.setWebChromeClient(new WebChromeClient() {
        });

        if (Build.VERSION.SDK_INT > 7) {
            settings.setPluginState(PluginState.ON);
        }

        final String mimeType = "text/html";
        final String encoding = "UTF-8";

        String ytID = YoutubeUtility.getYTIDFromYTUrl(MovieActivity.movie.trailerUrl);
        String html = "";

        Log.e("trailer url", MovieActivity.movie.trailerUrl);

        html = YoutubeUtility.getYTHTMLFromYTID(ytID);

        webView.loadDataWithBaseURL("", html, mimeType, encoding, "");
    }

    private void initEvent() {

    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            WebView.class.getMethod("onResume").invoke(webView);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            WebView.class.getMethod("onPause").invoke(webView);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}