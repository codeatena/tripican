package com.han.showtime.movie;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.han.showtime.R;
import com.han.showtime.activities.MovieActivity;

/**
 * Created by COREI3 on 5/5/2015.
 */
public class MovieDetailFragment extends Fragment {

    TextView txtStory;
    TextView txtDirector;
    TextView txtStars;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_movie_details, container, false);

        txtStory = (TextView) view.findViewById(R.id.story_textView);
        txtDirector = (TextView) view.findViewById(R.id.director_textView);
        txtStars = (TextView) view.findViewById(R.id.stars_textView);

        initValue();
        initEvent();

        return view;
    }

    private void initValue() {
        txtStory.setText(MovieActivity.movie.description);
        txtDirector.setText(MovieActivity.movie.getDirectorText());
        txtStars.setText(MovieActivity.movie.getActorText());
    }

    private void initEvent() {

    }
}
