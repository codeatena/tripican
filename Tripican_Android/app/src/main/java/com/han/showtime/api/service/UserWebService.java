package com.han.showtime.api.service;

import android.util.Log;

import com.han.api.MyWebService;
import com.han.api.OAuthToken;
import com.han.api.ParseResult;
import com.han.showtime.Consts;
import com.han.showtime.api.parseresult.CinemasParseResult;
import com.han.showtime.api.parseresult.MoviesParseResult;
import com.han.showtime.api.parseresult.ShowTimesParseResult;
import com.han.showtime.api.parseresult.StatesParseResult;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class UserWebService extends MyWebService {
	
    private static long ACCESS_TOKEN_TTL = 86400 * 60; //expiry of access token in seconds

    public static ParseResult readShowTimes(String strMovieID, String strCurDay, String strStateID, String strCinemaID) {
        ShowTimesParseResult parseResult = new ShowTimesParseResult();

        try {
            String url = URL_API + "movies/showtimes" + "?movie_id=" + strMovieID;

            if (strCurDay.equals(Integer.toString(Consts.TODAY))) {
                url += "&event_date=today";
            } else if (strCurDay.equals(Integer.toString(Consts.TOMORROW))) {
                url += "&event_date=tomorrow";
            }

            if (!strStateID.equals(Integer.toString(Consts.ALL))) {
                url += ("&state_id=" + strStateID);
            }
            if (!strCinemaID.equals(Integer.toString(Consts.ALL))) {
                url += ("&cinema_id=" + strCinemaID);
            }

            Log.e("showtimes url: ", url);
            String strResponse = get(url, null);
            Log.e("showtimes result: ", strResponse);

            parseResult.parse(strResponse);
            return parseResult;
        } catch (Exception e) {
            e.printStackTrace();
            parseResult.setResult(false, "No Internet Connection.");
            return parseResult;
        }
    }

    public static ParseResult readCinemas(String strStateID) {
        CinemasParseResult parseResult = new CinemasParseResult();

        try {

            String url = URL_API + "movies/cinemas";

            if (!strStateID.equals(Integer.toString(Consts.ALL))) {
                url = url + "?state_id=" + strStateID;
            }

            Log.e("cinemas url: ", url);

            String strResponse = get(url, null);
            Log.e("cinemas Result: ", strResponse);

            parseResult.parse(strResponse);

            return parseResult;
        } catch (Exception e) {

            e.printStackTrace();
            parseResult.setResult(false, "No Internet Connection.");

            return parseResult;
        }
    }

    public static ParseResult readStates(String countryCode) {
        StatesParseResult parseResult = new StatesParseResult();

        try {
            String url = URL_API + "list/states.json" + "?country=" + countryCode;
            Log.e("states url: ", url);

            String strResponse = get(url, null);
            Log.e("states Result: ", strResponse);

            parseResult.parse(strResponse);

            return parseResult;
        } catch (Exception e) {
            e.printStackTrace();
            parseResult.setResult(false, "No Internet Connection.");
            return parseResult;
        }
    }

    public static ParseResult readAllMovies(String strPageNum, String strStateID, String strCinemaID, String strCurDay) {

        MoviesParseResult parseResult = new MoviesParseResult();

        try {
            String url = "";

            if (strCurDay.equals(Integer.toString(Consts.TODAY))) {
                url = URL_API + "movies/showing-today.json" + "?page=" + strPageNum + "&limit=100";
            }
            if (strCurDay.equals(Integer.toString(Consts.TOMORROW))) {
                url = URL_API + "movies/showing-today.json" + "?page=" + strPageNum + "&limit=100" + "&date=tomorrow";
            }
            if (strCurDay.equals(Integer.toString(Consts.THIS_WEEK))) {
                url = URL_API + "movies/in-cinemas.json" + "?page=" + strPageNum + "&limit=100";
            }

            if (!strStateID.equals(Integer.toString(Consts.ALL))) {
                url = url + "&state_id=" + strStateID;
            }
            if (!strCinemaID.equals(Integer.toString(Consts.ALL))) {
                url = url + "&cinema_id=" + strCinemaID;
            }

            Log.e("movie list url: ", url);

            String strResponse = get(url, null);
            Log.e("movies list result: ", strResponse);

            parseResult.parse(strResponse);

            return parseResult;
        } catch (Exception e) {
            e.printStackTrace();
            parseResult.setResult(false, "No Internet Connection.");
            return parseResult;
        }
    }
	
	public static OAuthToken getAccessToken(String email, String password) {
		
		try {			
			String requestTokenUrl = URL_API + "oauth/request_token";
			Log.e("Request Token url: ", requestTokenUrl);

			String reqToken = MyWebService.get(requestTokenUrl, null);
	        Log.e("Request Token Resp: ", reqToken);

	        Map<String, String> tokens = splitQuery(reqToken);

	        OAuthToken requestToken = new OAuthToken(tokens.get("oauth_token"), tokens.get("oauth_token_secret"));
	        
	        String accessTokenUrl = URL_API + "oauth/access_token";
			Log.e("Access Token url: ", accessTokenUrl);

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("email", email));
            nameValuePairs.add(new BasicNameValuePair("password", password));
            nameValuePairs.add(new BasicNameValuePair("xoauth_token_ttl", Long.toString(ACCESS_TOKEN_TTL)));

	        String resp = post(accessTokenUrl, nameValuePairs, requestToken);
	        Log.e("Access Token Response: ", resp);

	        tokens = splitQuery(resp);

	        OAuthToken accessToken = new OAuthToken(tokens.get("oauth_token"), tokens.get("oauth_token_secret"));
	        Log.e("Access token: ", accessToken.toString());
	        
	        return accessToken;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}