package com.han.api;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;

import com.han.showtime.R;
import com.han.utility.DialogUtility;
import com.han.utility.NetworkUtility;

public class MyAsyncTask extends AsyncTask<String, Integer, ParseResult> {

	public static final int SUCCESS = 1;
	public static final int FAIL = -1;
	
	public Context parent;
	public ProgressDialog dlgLoading;
	public String title;
	
	public String curAction;

	@Override
	protected ParseResult doInBackground(String... arg0) {
		// TODO Auto-generated method stub
		return new ParseResult(false);
	}
	
	public MyAsyncTask(Context _parent) {
		parent = _parent;
		title = parent.getResources().getString(R.string.please_wait);
	}
	
	public MyAsyncTask(Context _parent, String _title) {
		parent = _parent;
		title = _title;
	}
	
	@Override
	protected void onPreExecute() {
		dlgLoading = new ProgressDialog(parent);
    	dlgLoading.setMessage(title);
    	dlgLoading.setCanceledOnTouchOutside(false);
    	dlgLoading.setCancelable(false);
    	
    	if (title != null) {
    		dlgLoading.show();
    	}
	}
	
	protected void onPostExecute(ParseResult pResult) {
		if (dlgLoading.isShowing())
			dlgLoading.dismiss();
		
		if (!pResult.isSuccess) {
			if (pResult.nType == ParseResult.INFO) {
				DialogUtility.show(parent, pResult.message);
			}
			if (pResult.nType == ParseResult.ALERT) {
				DialogUtility.showGeneralAlert(parent, "Alert", pResult.message);
			}
			if (pResult.nType == ParseResult.ERROR) {
				new AlertDialog.Builder(parent)

						.setTitle("Error")
						.setMessage(pResult.message)
						.setPositiveButton("OK", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								// continue with delete
								((Activity) parent).finish();
							}
						})
						.show();
			}
		}
	}
	
	protected void onProgressUpdate(Integer... progress) {
		
	}
}
