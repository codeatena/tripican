package com.han.showtime;

import com.han.showtime.model.Location;

import java.util.ArrayList;

/**
 * Created by COREI3 on 4/27/2015.
 */
public class GlobalData {

    public static GlobalData instance = null;

    public ArrayList<Location> arrStates;

    public static GlobalData getInstance() {

        if (instance == null) {
            instance = new GlobalData();
        }

        return instance;
    }

    public GlobalData() {
        arrStates = new ArrayList<Location>();
    }
}
