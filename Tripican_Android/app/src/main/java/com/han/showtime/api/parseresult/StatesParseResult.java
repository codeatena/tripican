package com.han.showtime.api.parseresult;

import com.han.api.ParseResult;
import com.han.showtime.model.Location;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by COREI3 on 4/27/2015.
 */
public class StatesParseResult extends ParseResult {

    public ArrayList<Location> arrLocation = new ArrayList<Location>();

    public void parse(String strResponse) {
        try {
            JSONObject jsonObj = new JSONObject(strResponse);
            boolean _isSuccess = (jsonObj.getInt("success") == 1) ? true: false;
            setResult(_isSuccess, "No Internet Connection.");
            if (isSuccess) {
                JSONArray jsonArr = jsonObj.getJSONArray("states");
                for (int i = 0; i < jsonArr.length(); i++) {
                    JSONObject jsonLocation = jsonArr.getJSONObject(i);
                    Location location = new Location().parse(jsonLocation);
                    arrLocation.add(location);
                }
                message = "success read cinemas";
            }
        } catch (Exception e) {
            e.printStackTrace();
            setResult(false, "No Internet Connection.");
        }
    }
}
