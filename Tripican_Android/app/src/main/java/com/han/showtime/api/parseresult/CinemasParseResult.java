package com.han.showtime.api.parseresult;

import com.han.api.ParseResult;
import com.han.showtime.model.Cinema;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by COREI3 on 5/5/2015.
 */
public class CinemasParseResult extends ParseResult {

    public ArrayList<Cinema> arrCinemas = new ArrayList<Cinema>();

    public void parse(String strResponse) {
        try {
            JSONObject jsonObj = new JSONObject(strResponse);
            boolean _isSuccess = (jsonObj.getInt("success") == 1) ? true: false;
            setResult(_isSuccess, "No Internet Connection.");
            if (isSuccess) {
                JSONArray jsonArr = jsonObj.getJSONArray("cinemas");
                for (int i = 0; i < jsonArr.length(); i++) {
                    JSONObject jsonCinema = jsonArr.getJSONObject(i);
                    Cinema cinema = new Cinema().parse(jsonCinema);
                    arrCinemas.add(cinema);
                }
                message = "success read cinemas";
            }
        } catch (Exception e) {
            e.printStackTrace();
            setResult(false, "No Internet Connection.");
        }
    }
}
