package com.han.showtime.model;

import org.json.JSONObject;

/**
 * Created by COREI3 on 5/5/2015.
 */
public class Cinema {

    public int id;
    public String centerName;
    public String addressLine1;
    public String addressLine2;
    public String addressCity;
    public String contactPhone;

    public Cinema() {
        id = 0;
        centerName = "";
        addressLine1 = "";
        addressLine2 = "";
        addressCity = "";
        contactPhone = "";
    }

    public Cinema parse(JSONObject jsonObj) {
        try {
            id = jsonObj.getInt("id");
            centerName = jsonObj.getString("centre_name");
            addressLine1 = jsonObj.getString("address_line_1");
            addressLine2 = jsonObj.getString("address_line_2");
            addressCity = jsonObj.getString("address_city");
            contactPhone = jsonObj.getString("contact_phone");

            return this;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
