package com.han.api; /**
 * Created by JosephT on 2/11/2015.
 */

/**
 * @author JosephT
 */
public class ApiError {

    private String message;
    private String code;

    public ApiError(String message, String code) {
        this.message = message;
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return String.format("(%s) %s", code, message);
    }

}
