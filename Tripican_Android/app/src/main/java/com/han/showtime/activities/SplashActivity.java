package com.han.showtime.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.han.showtime.R;
import com.han.showtime.api.asynctask.UserAsyncTask;
import com.han.utility.NetworkUtility;

public class SplashActivity extends Activity {

    private static int SPLASH_TIME_OUT = 3000;
    boolean isFinishSplashTimeOut;
    boolean isGettingStates;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        initWidget();
        initValue();
        initEvent();
    }

    private void initWidget() {

    }

    private void initValue() {
        isFinishSplashTimeOut = false;
        isGettingStates = false;

        new UserAsyncTask(this).execute(UserAsyncTask.ACTION_STATES, "NG");
    }

    private void initEvent() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                isFinishSplashTimeOut = true;
                goToMain();
            }
        }, SPLASH_TIME_OUT);
    }

    public void successGetStates() {
        isGettingStates = true;
        goToMain();
    }

    private void goToMain() {
        if (isFinishSplashTimeOut && isGettingStates) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
    }
}
