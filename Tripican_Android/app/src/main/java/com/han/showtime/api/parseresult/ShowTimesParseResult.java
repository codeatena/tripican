package com.han.showtime.api.parseresult;

import com.han.api.ParseResult;
import com.han.showtime.model.CinemaShowTimes;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by COREI3 on 5/8/2015.
 */
public class ShowTimesParseResult extends ParseResult {

    public ArrayList<CinemaShowTimes> arrCinemaShowTimes = new ArrayList<CinemaShowTimes>();

    public void parse(String strResponse) {
        try {
            JSONObject jsonObj = new JSONObject(strResponse);
            boolean _isSuccess = (jsonObj.getInt("success") == 1) ? true: false;
            setResult(_isSuccess, "No Internet Connection.");
            JSONArray jsonArr = jsonObj.getJSONArray("cinema_movie_showtimes");

            for (int i = 0; i < jsonArr.length(); i++) {
                JSONObject jsonCinemaShowTimes = jsonArr.getJSONObject(i);
                CinemaShowTimes cinemaShowTimes = new CinemaShowTimes().parse(jsonCinemaShowTimes);
                arrCinemaShowTimes.add(cinemaShowTimes);
            }

            message = "success read cinemas";
        } catch (Exception e) {
            e.printStackTrace();
            setResult(false, "No Internet Connection.");
        }
    }
}
