package com.han.showtime.model;

import org.json.JSONObject;

/**
 * Created by COREI3 on 4/27/2015.
 */
public class Location {
    public int id = 0;
    public String name = "";
    public double lat = 0.0f;
    public double lng = 0.0f;

    public Location() {
        id = 0;
        name = "";
        lat = 0.0f;
        lng = 0.0f;
    }

    public Location(int _id, String _name, double _lat, double _lng) {
        id = _id;
        name = _name;
        lat = _lat;
        lng = _lng;
    }

    public Location parse(JSONObject jsonObj) {
        try {
            id = jsonObj.getInt("id");
            name = jsonObj.getString("location_name");

            if (jsonObj.has("lat")) {
                lat = (jsonObj.isNull("lat")) ? 0.0f : jsonObj.getDouble("lat");
            }
            if (jsonObj.has("long")) {
                lat = (jsonObj.isNull("long")) ? 0.0f : jsonObj.getDouble("long");
            }

            return this;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
