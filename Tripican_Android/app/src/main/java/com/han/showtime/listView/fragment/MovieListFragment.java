package com.han.showtime.listView.fragment;

import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.han.showtime.R;
import com.han.showtime.activities.MovieActivity;
import com.han.showtime.api.asynctask.UserAsyncTask;
import com.han.showtime.listView.MyListAdapter;
import com.han.showtime.listView.MyListItem;
import com.han.showtime.model.Movie;

import java.util.ArrayList;

/**
 * Created by COREI3 on 4/28/2015.
 */
public class MovieListFragment extends ListFragment {

    private ArrayList<MyListItem> mItems;
    int curPage = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mItems = new ArrayList<MyListItem>();
        setListAdapter(new MyListAdapter(getActivity(), mItems));
    }

    public void refreshMovies(int stateID, int cinemaID, int nCurDay) {
        curPage = 1;
        mItems = new ArrayList<MyListItem>();
        new UserAsyncTask(getActivity()).execute(UserAsyncTask.ACTION_MOVIES, Integer.toString(curPage), Integer.toString(stateID), Integer.toString(cinemaID), Integer.toString(nCurDay));
    }

    public void addMovies(ArrayList<Movie> arrMovies) {
        for (Movie movie: arrMovies) {
            MyListItem movieItem = new MyListItem(R.layout.row_movie, movie);
            mItems.add(movieItem);
        }
        setListAdapter(new MyListAdapter(getActivity(), mItems));
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
//        MovieItem item = (MovieItem) mItems.get(position);
        Movie movie = (Movie) mItems.get(position).data;
        MovieActivity.movie = movie;

        getActivity().startActivity(new Intent(getActivity(), MovieActivity.class));
    }
}
