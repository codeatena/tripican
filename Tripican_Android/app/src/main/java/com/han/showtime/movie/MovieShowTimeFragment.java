package com.han.showtime.movie;

import android.app.Fragment;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.han.showtime.R;
import com.han.showtime.activities.MovieActivity;
import com.han.showtime.listView.MyListAdapter;
import com.han.showtime.listView.MyListItem;
import com.han.showtime.model.CinemaShowTimes;

import java.util.ArrayList;

/**
 * Created by COREI3 on 5/5/2015.
 */
public class MovieShowTimeFragment extends Fragment {

    private ArrayList<MyListItem> mItems = new ArrayList<MyListItem>();
    LinearLayout llytShowTimes;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_movie_showtimes, container, false);

        llytShowTimes = (LinearLayout) view.findViewById(R.id.showtimes_linearLayout);

        initValue();
        initEvent();

        return view;
    }

    private void initValue() {

    }

    private void initEvent() {

    }

    public void setShowTimes() {

        mItems = new ArrayList<MyListItem>();
        for (CinemaShowTimes cinemaShowTimes: MovieActivity.arrCinemaShowtimes) {
            mItems.add(new MyListItem(R.layout.row_showtime, cinemaShowTimes));
        }

        MyListAdapter adapter = new MyListAdapter(getActivity(), mItems);
        for (int i = 0; i < adapter.getCount(); i++) {
            View view = adapter.getView(i, null, llytShowTimes);
            llytShowTimes.addView(view);
        }
    }
}