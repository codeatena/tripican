package com.han.showtime.model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by COREI3 on 2/6/2015.
 */
public class Movie {

    public int id = 0;
    public String title = "";
    public String description = "";
    public String poster = "";
    public ArrayList<String> arrGenres = new ArrayList<String>();
    public double imdb = 8.0f;
    public ArrayList<String> arrActors = new ArrayList<String>();
    public ArrayList<String> arrDirectors = new ArrayList<String>();
    public String trailerUrl = "";
    public int duration = 0;

    public Movie parse(JSONObject jsonObj) {
        try {
            id = jsonObj.getInt("id");
            title = jsonObj.getString("title");

            if (jsonObj.has("description")) {
                if (!jsonObj.isNull("description")) {
                    description = jsonObj.getString("description");
                }
            }
            if (jsonObj.has("poster")) {
                if (!jsonObj.isNull("poster")) {
                    poster = jsonObj.getString("poster");
                }
            }
            if (jsonObj.has("duration")) {
                if (!jsonObj.isNull("duration")) {
                    duration = jsonObj.getInt("duration");
                }
            }

            arrGenres = new ArrayList<String>();
            arrActors = new ArrayList<String>();
            arrDirectors = new ArrayList<String>();

            if (jsonObj.has("genre")) {
                if (!jsonObj.isNull("genre")) {
                    JSONArray jsonArrGenre = jsonObj.getJSONArray("genre");
                    for (int i = 0; i < jsonArrGenre.length(); i++) {
                        String genre = jsonArrGenre.getString(i);
                        arrGenres.add(genre);
                    }
                }
            }
            if (jsonObj.has("actor")) {
                if (!jsonObj.isNull("actor")) {
                    JSONArray jsonArrActors = jsonObj.getJSONArray("actor");
                    for (int i = 0; i < jsonArrActors.length(); i++) {
                        String actor = jsonArrActors.getString(i);
                        arrActors.add(actor);
                    }
                }
            }
            if (jsonObj.has("director")) {
                if (!jsonObj.isNull("director")) {
                    JSONArray jsonArrDirectors = jsonObj.getJSONArray("director");
                    for (int i = 0; i < jsonArrDirectors.length(); i++) {
                        String director = jsonArrDirectors.getString(i);
                        arrDirectors.add(director);
                    }
                }
            }
            if (jsonObj.has("trailer_url")) {
                if (!jsonObj.isNull("trailer_url")) {
                    JSONArray jsonArray = jsonObj.getJSONArray("trailer_url");
                    trailerUrl = jsonArray.get(0).toString();
                }
            }

            return this;
        } catch (Exception e) {
            e.printStackTrace();
            return this;
        }
    }

    public String getGenreText() {
        int i = 0;
        String val = "";
        for (String str: arrGenres) {
            if (i == 0) {
                val = str;
            } else {
                val += (", " + str);
            }
            i++;
        }
        return val;
    }

    public String getActorText() {
        int i = 0;
        String val = "";
        for (String str: arrActors) {
            if (i == 0) {
                val = str;
            } else {
                val += (", " + str);
            }
            i++;
        }
        return val;
    }

    public String getDirectorText() {
        int i = 0;
        String val = "";
        for (String str: arrDirectors) {
            if (i == 0) {
                val = str;
            } else {
                val += (", " + str);
            }
            i++;
        }
        return val;
    }
}