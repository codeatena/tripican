package com.han.showtime.activities;

import android.app.Activity;
import android.graphics.Bitmap;
import android.media.Image;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.han.showtime.R;
import com.han.showtime.api.asynctask.UserAsyncTask;
import com.han.showtime.model.CinemaShowTimes;
import com.han.showtime.model.Movie;
import com.han.showtime.movie.MovieDetailFragment;
import com.han.showtime.movie.MovieShowTimeFragment;
import com.han.showtime.movie.MovieTrailerFragment;
import com.han.utility.ImageUtility;
import com.han.utility.ScreenUtility;
import com.koushikdutta.async.future.Future;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;

public class MovieActivity extends Activity {

    ImageView imgBack;
//    ImageView imgSearch;
    TextView txtTitle;

    TextView txtImdb;
    TextView txtRuntime;

    ImageView imgThumb;

    TextView txtShowTime;
    TextView txtTrailer;
    TextView txtDetails;

    LinearLayout llytShowTime;
    LinearLayout llytTrailer;
    LinearLayout llytDetail;

    MovieShowTimeFragment movieShowTimeFragment;
    MovieTrailerFragment movieTrailerFragment;
    MovieDetailFragment movieDetailFragment;

    final int SHOWTIME = 0;
    final int TRAILER = 1;
    final int DETAILS = 2;

    public static Movie movie;
    public static ArrayList<CinemaShowTimes> arrCinemaShowtimes = new ArrayList<CinemaShowTimes>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);

        initWidget();
        initValue();
        initEvent();
    }

    private void initWidget() {

        imgBack = (ImageView) findViewById(R.id.menu_imageView);
//        imgSearch = (ImageView) findViewById(R.id.search_imageView);
        txtTitle = (TextView) findViewById(R.id.title_textView);

        movieShowTimeFragment = new MovieShowTimeFragment();
        movieTrailerFragment = new MovieTrailerFragment();
        movieDetailFragment = new MovieDetailFragment();

        txtImdb = (TextView) findViewById(R.id.imdb_textView);
        txtRuntime = (TextView) findViewById(R.id.runtime_textView);

        imgThumb = (ImageView) findViewById(R.id.thumb_imageView);

        txtShowTime = (TextView) findViewById(R.id.showtime_textView);
        txtTrailer = (TextView) findViewById(R.id.trailer_textView);
        txtDetails = (TextView) findViewById(R.id.details_textView);

        llytShowTime = (LinearLayout) findViewById(R.id.showtime_linearLayout);
        llytTrailer = (LinearLayout) findViewById(R.id.trailer_linearLayout);
        llytDetail = (LinearLayout) findViewById(R.id.detail_linearLayout);
    }

    private void initValue() {

        txtTitle.setText(movie.title);

        Log.e("state and cinema id: ", Integer.toString(MainActivity.nCurStateID) + ", " + Integer.toString(MainActivity.nCurCinemaID));

        Ion.with(this)
                .load(movie.poster)
                .asBitmap()
                .setCallback(new FutureCallback<Bitmap>() {
                    @Override
                    public void onCompleted(Exception e, Bitmap result) {
                        int screenWidth = ScreenUtility.getScreenWidth(MovieActivity.this);
//                        int screenHieght = ScreenUtility.getScreenHeight(MovieActivity.this);

//                        Log.e("image width and height", Integer.toString(result.getWidth()) + ", " + Integer.toString(result.getHeight()));
//                        int height = result.getHeight() * screenWidth / result.getWidth();
//
//                        Bitmap bmp = ImageUtility.getResizedBitmap(result, screenWidth, height);

                        imgThumb.setImageBitmap(result);
                    }
                });

        txtImdb.setText(String.format("IMDB RATING: %.1f", movie.imdb));
        txtRuntime.setText(String.format("RUN TIME: %d MIN", movie.duration));

        new UserAsyncTask(this).execute(UserAsyncTask.ACTION_SHOWTIMES, Integer.toString(movie.id),
                Integer.toString(MainActivity.nCurDay), Integer.toString(MainActivity.nCurStateID), Integer.toString(MainActivity.nCurCinemaID));

        getFragmentManager().beginTransaction()
                .replace(R.id.showtime_linearLayout, movieShowTimeFragment)
                .commit();
        getFragmentManager().beginTransaction()
                .replace(R.id.trailer_linearLayout, movieTrailerFragment)
                .commit();
        getFragmentManager().beginTransaction()
                .replace(R.id.detail_linearLayout, movieDetailFragment)
                .commit();

        onTab(SHOWTIME);
    }

    private void initEvent() {
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
//        imgSearch.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });
        txtShowTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTab(SHOWTIME);
            }
        });
        txtTrailer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTab(TRAILER);
            }
        });
        txtDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTab(DETAILS);
            }
        });
    }

    public void onTab(int nIndex) {

        txtShowTime.setTextColor(0xffffffff);
        txtTrailer.setTextColor(0xffffffff);
        txtDetails.setTextColor(0xffffffff);

        llytShowTime.setVisibility(View.GONE);
        llytTrailer.setVisibility(View.GONE);
        llytDetail.setVisibility(View.GONE);

        if (nIndex == SHOWTIME) {
            txtShowTime.setTextColor(0xfff68634);
            llytShowTime.setVisibility(View.VISIBLE);
//            getFragmentManager().beginTransaction()
//                    .replace(R.id.showtime_linearLayout, movieShowTimeFragment)
//                    .commit();
        }
        if (nIndex == TRAILER) {
            txtTrailer.setTextColor(0xfff68634);
            llytTrailer.setVisibility(View.VISIBLE);
//            getFragmentManager().beginTransaction()
//                    .replace(R.id.trailer_linearLayout, movieTrailerFragment)
//                    .commit();
        }
        if (nIndex == DETAILS) {
            txtDetails.setTextColor(0xfff68634);
            llytDetail.setVisibility(View.VISIBLE);
//            getFragmentManager().beginTransaction()
//                    .replace(R.id.detail_linearLayout, movieDetailFragment)
//                    .commit();
        }
    }

    public void successReadShowTime(ArrayList<CinemaShowTimes> _arrCinemaShowtimes) {
        arrCinemaShowtimes = _arrCinemaShowtimes;
        movieShowTimeFragment.setShowTimes();
    }
}