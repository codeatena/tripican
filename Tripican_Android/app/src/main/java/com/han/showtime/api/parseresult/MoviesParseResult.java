package com.han.showtime.api.parseresult;

import com.han.api.ParseResult;
import com.han.showtime.model.Movie;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by COREI3 on 4/27/2015.
 */
public class MoviesParseResult extends ParseResult {

    public ArrayList<Movie> arrMovies = new ArrayList<Movie>();

    public void parse(String strResponse) {
        try {
            JSONObject jsonObj = new JSONObject(strResponse);
            boolean _isSuccess = (jsonObj.getInt("success") == 1) ? true: false;
            setResult(_isSuccess, "No Internet Connection.");
            if (isSuccess) {
                JSONArray jsonArr = jsonObj.getJSONArray("movies");
                for (int i = 0; i < jsonArr.length(); i++) {
                    JSONObject jsonMovie = jsonArr.getJSONObject(i);
                    Movie movie = new Movie().parse(jsonMovie);
                    arrMovies.add(movie);
                }
                message = "success read cinemas";
            }
        } catch (Exception e) {
            e.printStackTrace();
            setResult(false, "No Internet Connection.");
        }
    }
}
