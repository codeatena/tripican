package com.han.showtime.spinner;

/**
 * Created by COREI3 on 5/5/2015.
 */
public class MySpinnerItem {

    public Object data;

    public int backLayoutID;
    public int dropdownLayoutID;

    public MySpinnerItem(int _backLayoutID, int _dropdownLayoutID, Object _data) {
        backLayoutID = _backLayoutID;
        dropdownLayoutID = _dropdownLayoutID;
        data = _data;
    }
}