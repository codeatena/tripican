package com.han.showtime.model;

import android.util.Log;

import org.json.JSONObject;

/**
 * Created by COREI3 on 5/5/2015.
 */
public class ShowTime {
    public int id;
    public String eventDate;
    public String eventTime;
    public String url;

    public ShowTime() {
        id = 0;
        eventDate = "";
        eventTime = "";
        url = "";
    }

    public ShowTime parse(JSONObject jsonObj) {
        try {
            id = jsonObj.getInt("id");
            eventDate = jsonObj.getString("event_date");
            eventTime = jsonObj.getString("event_time");
            eventTime = eventTime.substring(0, eventTime.length() - 3);
            int k = eventTime.indexOf(":");

            int h = Integer.parseInt(eventTime.substring(0, k));
            if (h > 12) {
                h -= 12;
                eventTime = String.format("%d", h) + eventTime.substring(k) + "pm";
            } else {
                eventTime = String.format("%d", h) + eventTime.substring(k) + "am";
            }
            url = jsonObj.getString("url");
            return this;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
