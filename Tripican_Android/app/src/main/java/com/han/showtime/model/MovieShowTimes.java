package com.han.showtime.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by COREI3 on 5/5/2015.
 */
public class MovieShowTimes {

    public Movie movie = new Movie();
    public ArrayList<ShowTime> arrShowTimes = new ArrayList<ShowTime>();
    public String purchaseUrl;

    public MovieShowTimes parse(JSONObject jsonObj) {
        try {
            movie = new Movie().parse(jsonObj.getJSONObject("movie"));
            JSONArray jsonArrShowTimes = jsonObj.getJSONArray("showtimes");

            arrShowTimes = new ArrayList<ShowTime>();

            for (int i = 0; i < jsonArrShowTimes.length(); i++) {
                JSONObject jsonShowTime = jsonArrShowTimes.getJSONObject(i);
                ShowTime showTime = new ShowTime().parse(jsonShowTime);
                arrShowTimes.add(showTime);
            }

            purchaseUrl = jsonObj.getString("purchase_url");
            Log.e("purchase url: ", purchaseUrl);

            return this;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getShowTimesText() {
        int i = 0;
        String val = "";
        for (ShowTime showTime: arrShowTimes) {
            if (i == 0) {
                val = showTime.eventTime;
            } else {
                val += (", " + showTime.eventTime);
            }
            i++;
        }
        return val;
    }
}
