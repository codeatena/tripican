package com.han.showtime.listView;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.han.showtime.R;
import com.han.showtime.model.CinemaShowTimes;
import com.han.showtime.model.Movie;
import com.han.showtime.model.MovieShowTimes;
import com.han.utility.DialogUtility;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;

public class MyListAdapter extends ArrayAdapter<MyListItem> {

    public MyListAdapter(Context context, ArrayList<MyListItem> listItems) {
        super(context, 0, listItems);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        LayoutInflater inflater = LayoutInflater.from(getContext());

        MyListItem item = getItem(position);

        if (item instanceof MyListItem) {

            if (view == null) {
                view = inflater.inflate(item.layoutID, parent, false);
            }

            Object data = item.data;

            if (data instanceof CinemaShowTimes) {

                final CinemaShowTimes cinemaShowTimes = (CinemaShowTimes) data;
                TextView txtTitle = (TextView) view.findViewById(R.id.showtime_title_textView);
                TextView txtAddress = (TextView) view.findViewById(R.id.showtime_address_textView);
                TextView txtTime = (TextView) view.findViewById(R.id.showtime_time_textView);
                Button btnGetTickets = (Button) view.findViewById(R.id.get_tickets_button);
                txtTitle.setText(cinemaShowTimes.cinema.centerName);
                txtAddress.setText(cinemaShowTimes.cinema.addressLine1);
                txtTime.setText("");

                final String purchaseUrl = cinemaShowTimes.arrMovieShowTimes.get(0).purchaseUrl;

                if (purchaseUrl.equals("null")) {
                    btnGetTickets.setVisibility(View.GONE);
                } else {
                    btnGetTickets.setVisibility(View.VISIBLE);
                }

                btnGetTickets.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(purchaseUrl));
                        getContext().startActivity(browserIntent);
                    }
                });

                if (cinemaShowTimes.arrMovieShowTimes.size() > 0) {
                    txtTime.setText(cinemaShowTimes.arrMovieShowTimes.get(0).getShowTimesText());
                }
            } else if (data instanceof Movie) {
                ImageView imgMovie = (ImageView) view.findViewById(R.id.movie_imageView);
                TextView txtTitle = (TextView) view.findViewById(R.id.title_textView);
                TextView txtGenre = (TextView) view.findViewById(R.id.genre_textView);
                TextView txtImdb = (TextView) view.findViewById(R.id.imdb_textView);

                Movie movie = (Movie) item.data;

                txtTitle.setText(movie.title);
                txtGenre.setText(movie.getGenreText());
                txtImdb.setText("Imdb : " + String.format("%.1f", movie.imdb));

                Ion.with(imgMovie).load(movie.poster);
            }
        }

        return view;
    }
}