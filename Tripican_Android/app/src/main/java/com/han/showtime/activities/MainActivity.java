package com.han.showtime.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.han.showtime.Consts;
import com.han.showtime.GlobalData;
import com.han.showtime.R;
import com.han.showtime.api.asynctask.UserAsyncTask;
import com.han.showtime.listView.fragment.MovieListFragment;
import com.han.showtime.model.Cinema;
import com.han.showtime.model.Location;
import com.han.showtime.model.Movie;
import com.han.showtime.spinner.MySpinnerAdapter;
import com.han.showtime.spinner.MySpinnerItem;
import com.navdrawer.SimpleSideDrawer;

import java.util.ArrayList;

public class MainActivity extends Activity {

//    ImageView imgMenuButton;

    Spinner spnState;

    ArrayList<MySpinnerItem> itemsState = new ArrayList<MySpinnerItem>();
    ArrayList<MySpinnerItem> itemsCinema = new ArrayList<MySpinnerItem>();

    TextView txtToday;
    TextView txtTomorrow;
    TextView txtThisWeek;

    Spinner spnCinema;

    MovieListFragment movieListFragment = new MovieListFragment();

    public static int nCurDay;
    public static int nCurStateID;
    public static int nCurCinemaID;

    TextView txtCannotFindMovies;

//    SimpleSideDrawer slidngMenu;

//    RelativeLayout rlytAboutUs;
//    RelativeLayout rlytSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initWidget();
        initValue();
        initEvent();
    }

    private void initWidget() {

//        slidngMenu = new SimpleSideDrawer(this);
//        slidngMenu.setLeftBehindContentView(R.layout.main_menu);
//
//        rlytAboutUs = (RelativeLayout) slidngMenu.findViewById(R.id.about_us_relativeLayout);
//        rlytSettings = (RelativeLayout) slidngMenu.findViewById(R.id.settings_relativeLayout);

//        imgMenuButton = (ImageView) findViewById(R.id.menu_imageView);

        spnState = (Spinner) findViewById(R.id.state_spinner);

        txtToday = (TextView) findViewById(R.id.today_textView);
        txtTomorrow = (TextView) findViewById(R.id.tomorrow_textView);
        txtThisWeek = (TextView) findViewById(R.id.this_week_textView);

        txtCannotFindMovies = (TextView) findViewById(R.id.cannot_find_movies_textView);

        spnCinema = (Spinner) findViewById(R.id.cinema_spinner);

        getFragmentManager().beginTransaction()
                .replace(R.id.movies_linearLayout, movieListFragment)
                .commit();
    }

    private void initValue() {
        nCurDay = Consts.ALL;
        nCurCinemaID = Consts.ALL;
        nCurStateID = Consts.ALL;

        txtCannotFindMovies.setVisibility(View.GONE);

        refreshStates();
        setCurDay(Consts.TODAY);
    }

    public void refreshStates() {
//        if (GlobalData.getInstance().arrStates.size() > 0) {

        Location promptLocation = new Location(Consts.ALL, "Select a city", 0.0f, 0.0f);
        Location allLocation = new Location(Consts.ALL, "ALL LOCATIONS", 0.0f, 0.0f);

        MySpinnerItem promptItem = new MySpinnerItem(R.layout.row_simple_spinner_back, R.layout.row_simple_spinner_prompt, promptLocation);
        MySpinnerItem topItem = new MySpinnerItem(R.layout.row_simple_spinner_back, R.layout.row_simple_spinner_dropdown, allLocation);

        itemsState.add(promptItem);
        itemsState.add(topItem);

        for (Location location: GlobalData.getInstance().arrStates) {
            MySpinnerItem mySpinnerItem = new MySpinnerItem(R.layout.row_simple_spinner_back, R.layout.row_simple_spinner_dropdown, location);
            itemsState.add(mySpinnerItem);
        }

        spnState.setAdapter(new MySpinnerAdapter(this, itemsState));
        spnState.setSelection(1);
//            Location location = GlobalData.getInstance().arrStates.get(0);
//            new UserAsyncTask(this).execute(UserAsyncTask.ACTION_CINEMAS, Integer.toString(location.id));
//        }
    }

    private void initEvent() {
//        rlytAboutUs.setOnClickListener(new Button.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                slidngMenu.toggleLeftDrawer();
//            }
//        });
//        rlytSettings.setOnClickListener(new Button.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                slidngMenu.toggleLeftDrawer();
//            }
//        });
//        imgMenuButton.setOnClickListener(new Button.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                slidngMenu.toggleLeftDrawer();
//            }
//        });
        txtToday.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCurDay(Consts.TODAY);
            }
        });

        txtTomorrow.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCurDay(Consts.TOMORROW);
            }
        });
        txtThisWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCurDay(Consts.THIS_WEEK);
            }
        });

        spnState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Location location = (Location) itemsState.get(position).data;
                itemsCinema = new ArrayList<MySpinnerItem>();
                nCurStateID = location.id;
                new UserAsyncTask(MainActivity.this).execute(UserAsyncTask.ACTION_CINEMAS, Integer.toString(location.id));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spnCinema.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Location location = (Location) itemsState.get(spnState.getSelectedItemPosition()).data;
                Cinema cinema = (Cinema) itemsCinema.get(position).data;
                nCurCinemaID = cinema.id;
                movieListFragment.refreshMovies(location.id, cinema.id, nCurDay);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void cleanAllDay() {
        txtToday.setTextColor(0xff545454);
        txtTomorrow.setTextColor(0xff545454);
        txtThisWeek.setTextColor(0xff545454);
    }

    private void setCurDay(int nDay) {
        if (nCurDay == nDay) return;

        cleanAllDay();
        if (nDay == Consts.TODAY) {
            txtToday.setTextColor(0xfff68634);
        }
        if (nDay == Consts.TOMORROW) {
            txtTomorrow.setTextColor(0xfff68634);
        }
        if (nDay == Consts.THIS_WEEK) {
            txtThisWeek.setTextColor(0xfff68634);
        }

        Location location = (Location) itemsState.get(spnState.getSelectedItemPosition()).data;
        int k = spnCinema.getSelectedItemPosition();
        nCurDay = nDay;

        if (k < 0) return;

        Cinema cinema = (Cinema) itemsCinema.get(spnCinema.getSelectedItemPosition()).data;
        k = cinema.id;

        movieListFragment.refreshMovies(location.id, k, nCurDay);
    }

    public void addCinemas(ArrayList<Cinema> arrCinemas) {

        Cinema promptCinema = new Cinema();
        promptCinema.centerName = "Select a cinema";
        promptCinema.id = -1;

        Cinema tmpCinema = new Cinema();
        tmpCinema.centerName = "ALL CINEMAS";
        tmpCinema.id = -1;

        itemsCinema.add(new MySpinnerItem(R.layout.row_simple_spinner_back, R.layout.row_simple_spinner_prompt, promptCinema));
        itemsCinema.add(new MySpinnerItem(R.layout.row_simple_spinner_back, R.layout.row_simple_spinner_dropdown, tmpCinema));

        for (Cinema cinema: arrCinemas) {
            MySpinnerItem mySpinnerItem = new MySpinnerItem(R.layout.row_simple_spinner_back, R.layout.row_simple_spinner_dropdown, cinema);
            itemsCinema.add(mySpinnerItem);
        }

//        Location location = (Location) itemsCinema.get(spnState.getSelectedItemPosition()).data;
        spnCinema.setAdapter(new MySpinnerAdapter(this, itemsCinema));
//        movieListFragment.refreshMovies(location.id, 0, nCurDay);
        spnCinema.setSelection(1);
    }

    public void addMovies(ArrayList<Movie> arrMovies) {
        if (arrMovies.size() == 0) {
            txtCannotFindMovies.setVisibility(View.VISIBLE);
        } else {
            txtCannotFindMovies.setVisibility(View.GONE);
        }
        movieListFragment.addMovies(arrMovies);
    }
}