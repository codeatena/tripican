package com.han.showtime.listView;

public class MyListItem {
    public Object data;
	public int layoutID;

    public MyListItem(int _layoutID, Object _data) {
        layoutID = _layoutID;
        data = _data;
    }
}